module.exports = {
  releasenote: [
    'MIL/what-is-MIL',
    {
      type: 'category',
      collapsed: false,
      label: 'Release Note',
      items: [
        {
          type: 'category',
          label: 'MIL 1.1.0',
          collapsed: false,
          items: [
            'MIL/MIL110/MIL110_UC-2100_UC-2100-W_ReleaseNote',
            'MIL/MIL110/MIL110_UC-3100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-5100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100-ME-T_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100A-ME-T_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8200_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8410A_ReleaseNote',
          ],
        },
      ],
    },
  ]
};
