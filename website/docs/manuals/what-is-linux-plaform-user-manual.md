---
title: Introduction
slug: /manuals/
---

## x86 Linux drivers & SDK

**x86 Linux drivers & SDK** provides Moxa Linux x86 computer develop user guide, 
including peripheral drivers porting guide, necessary setting up on Linux distributions, 
driver porting, I/O controlling example code, etc.

### I/O Driver User Manual

**Computer I/O Driver User Manual** provides for specific x86 computer’s develop user guide 
for peripheral controlling, troubleshooting, and necessary information.

### Wireless Module Driver User Manual

**Wireless Module Driver User Manual** provides RF modules user guide for cellular and WiFi modules, 
including dial-up steps, troubleshooting, and drivers porting guide (if necessary).

### Porting Guide on Linux Distributions

**Porting Guide on Linux Distributions** provides develop user guide for popular Linux Distributions, 
including Debian, Ubuntu and CentOS. And provides common example code for controlling peripheral on specific platforms.
