---
title: UC-8410A
---

## MIL 1.1.0 for UC-8410A Series

| **OS Image Version** | Build No. | Debian Ver. | Kernel Ver.                    | Release Date |
|:-------------------- | --------- | ----------- | ------------------------------ | ------------ |
| v4.1.2               | 22030217  | 9.13        | linux 4.4.285-cip63-rt36-moxa8 | 4/18/2022    |

### Table of contents

1. [Moxa Package Change Log](#moxa-package-change-log)
2. [Debian Software Package Change Log](#debian-software-package-change-log)
3. [Debian Security Patch](#debian-security-patch)
4. [Kernel Security Patch](#kernel-security-patch)

### Moxa Package Change Log

For detail change log of each package, refers to [UC-8410A MIL 1.1 Change Log](changelog/MIL110_UC-8410A_220302_045209.zip)

| Package                                              | Type    | Version                                                 | Major Reason                                                                                                                                |
| ---------------------------------------------------- | ------- | ------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| moxa-auto-mountd                                     | Add     | '1.1.0'                                                 | USB/SD auto-mounting utility                                                                                                                |
| moxa-mxio-library                                    | Add     | '2.12.5+21032316+deb9'                                  | User-friendly I/O programming library to reduce development times and transfer operations in retrieving data from Moxa ioLogik E1200 Series |
| moxa-mil-base-system-armhf                           | Add     | '1.0.0+deb9'                                            | Unified MIL configuration                                                                                                                   |
| linux-headers-4.4.0-cip-rt-moxa-ls102xa              | Upgrade | '4.4.201+1.1.6+deb9' to '4.4.285-cip63-rt36-moxa8+deb9' | Pre-install kernel header for building driver                                                                                               |
| linux-kbuild-4.4.0-cip-rt-moxa-ls102xa               | Upgrade | '4.4.201+1.1.6+deb9' to '4.4.285-cip63-rt36-moxa8+deb9' | Pre-install kbuild for building driver                                                                                                      |
| moxa-cellular-utils                                  | Upgrade | '2.10.5' to '2.11.1'                                    | Display signal strength LED only if connection to internet is available                                                                     |
| moxa-cellular-signald                                | Upgrade | '2.10.5' to '2.11.1'                                    | Refers to moxa-cellular-utils                                                                                                               |
| moxa-version                                         | Upgrade | '1.1.0+deb9u2' to '1.2.0+deb9'                          | Add MIL version to  'mx-ver' version display utility                                                                                        |
| moxa-wifi-signald                                    | Upgrade | '1.8.6' to '1.8.8'                                      | Support additional Moxa computer models<br />                                                                                                |
| moxa-wifi-utils                                      | Upgrade | '1.8.6' to '1.8.8'                                      | Refers to moxa-wifi-utils                                                                                                                   |
| mxp-common-udev-rules                                | Upgrade | '1.0.3' to '1.1.0'                                      | Update udev rule of Moxa platform                                                                                                           |
| uc8410a-base-system                                  | Upgrade | '1.2.1+deb9' to '1.4.1'                                 | Update minimum required version of packages                                                                                                 |
| uc8410a-kernel                                       | Upgrade | '4.4.201+1.1.6+deb9' to '4.4.285-cip63-rt36-moxa8+deb9' | Add USB Moxa UPORT Serial Driver                                                                                                            |
| uc8410a-modules                                      | Upgrade | '4.4.201+1.1.6+deb9' to '4.4.285-cip63-rt36-moxa8+deb9' | Add USB Moxa UPORT Serial Driver                                                                                                            |
| uc8410a-setinterface                                 | Upgrade | '1.2.1+deb9' to '1.4.0'                                 | Update minimum required version of packages                                                                                                 |
| uc8410a-t2                                           | Upgrade | '1.2.1+deb9' to '1.4.0'                                 | Update minimum required version of packages                                                                                                 |
| moxa-nport-real-tty-utils                            | Upgrade | 1.19+1.0.0+chn1' to '1.19+1.0.0'                        | Update driver from CV to generic version                                                                                                    |
| cryptodev-linux-4.4.0-cip-rt-moxa-ls102xa            | Remove  |                                                         |                                                                                                                                             |
| moxa-nport-real-tty-driver-4.4.0-cip-rt-moxa-ls102xa | Remove  |                                                         |                                                                                                                                             |
| moxa-rfs-files                                       | Remove  |                                                         |                                                                                                                                             |

### Debian Software Package Change Log

For detail change log of each package, refers to [UC-8410A MIL 1.1 Change Log](changelog/MIL110_UC-8410A_220302_045209.zip)

| Package          | Type    | Version                                                           |
| ---------------- | ------- | ----------------------------------------------------------------- |
| libevent-2.0-5   | Add     | '2.0.21-stable-3'                                                 |
| libjansson4      | Add     | '2.9-1'                                                           |
| libtss2-esys0    | Add     | '2.4.0-1-moxa1'                                                   |
| lldpd            | Add     | '0.9.6-1'                                                         |
| tpm2-tools       | Add     | '4.1.1-1-moxa1'                                                   |
| apache2          | Upgrade | '2.4.25-3+deb9u9' to '2.4.25-3+deb9u11'                           |
| apache2-bin      | Upgrade | '2.4.25-3+deb9u9' to '2.4.25-3+deb9u11'                           |
| apache2-data     | Upgrade | '2.4.25-3+deb9u9' to '2.4.25-3+deb9u11'                           |
| apache2-utils    | Upgrade | '2.4.25-3+deb9u9' to '2.4.25-3+deb9u11'                           |
| apt              | Upgrade | '1.4.10' to '1.4.11'                                              |
| apt-utils        | Upgrade | '1.4.10' to '1.4.11'                                              |
| busybox          | Upgrade | '1:1.22.0-19+b3' to '1:1.22.0-19+deb9u2'                          |
| ca-certificates  | Upgrade | '20200601\~deb9u1' to '20200601\~deb9u2'                          |
| cron             | Upgrade | '3.0pl1-128+deb9u1' to '3.0pl1-128+deb9u2'                        |
| e2fslibs         | Upgrade | '1.43.4-2+deb9u1' to '1.43.4-2+deb9u2'                            |
| e2fsprogs        | Upgrade | '1.43.4-2+deb9u1' to '1.43.4-2+deb9u2'                            |
| isc-dhcp-client  | Upgrade | '4.3.5-3+deb9u1' to '4.3.5-3+deb9u2'                              |
| isc-dhcp-common  | Upgrade | '4.3.5-3+deb9u1' to '4.3.5-3+deb9u2'                              |
| libapt-inst2.0   | Upgrade | '1.4.10' to '1.4.11'                                              |
| libapt-pkg5.0    | Upgrade | '1.4.10' to '1.4.11'                                              |
| libbsd0          | Upgrade | '0.8.3-1' to '0.8.3-1+deb9u1'                                     |
| libcomerr2       | Upgrade | '1.43.4-2+deb9u1' to '1.43.4-2+deb9u2'                            |
| libcurl3         | Upgrade | '7.52.1-5+deb9u10' to '7.52.1-5+deb9u16'                          |
| libcurl3-gnutls  | Upgrade | '7.52.1-5+deb9u10' to '7.52.1-5+deb9u16'                          |
| libdns-export162 | Upgrade | '1:9.10.3.dfsg.P4-12.3+deb9u6' to '1:9.10.3.dfsg.P4-12.3+deb9u10' |
| libelf1          | Upgrade | '0.168-1' to '0.168-1+deb9u1'                                     |
| libgmp10         | Upgrade | '2:6.1.2+dfsg-1' to '2:6.1.2+dfsg-1+deb9u1'                       |
| libgnutls30      | Upgrade | '3.5.8-5+deb9u5' to '3.5.8-5+deb9u6'                              |
| libgssapi-krb5-2 | Upgrade | '1.15-1+deb9u1' to '1.15-1+deb9u3'                                |
| libhogweed4      | Upgrade | '3.3-1+b2' to '3.3-1+deb9u1'                                      |
| libicu57         | Upgrade | '57.1-6+deb9u4' to '57.1-6+deb9u5'                                |
| libisc-export160 | Upgrade | '1:9.10.3.dfsg.P4-12.3+deb9u6' to '1:9.10.3.dfsg.P4-12.3+deb9u10' |
| libjson-c3       | Upgrade | '0.12.1-1.1' to '0.12.1-1.1+deb9u1'                               |
| libk5crypto3     | Upgrade | '1.15-1+deb9u1' to '1.15-1+deb9u3'                                |
| libkrb5-3        | Upgrade | '1.15-1+deb9u1' to '1.15-1+deb9u3'                                |
| libkrb5support0  | Upgrade | '1.15-1+deb9u1' to '1.15-1+deb9u3'                                |
| libldap-2.4-2    | Upgrade | '2.4.44+dfsg-5+deb9u4' to '2.4.44+dfsg-5+deb9u8'                  |
| libldap-common   | Upgrade | '2.4.44+dfsg-5+deb9u4' to '2.4.44+dfsg-5+deb9u8'                  |
| liblz4-1         | Upgrade | '0.0\~r131-2+b1' to '0.0\~r131-2+deb9u1'                          |
| libnettle6       | Upgrade | '3.3-1+b2' to '3.3-1+deb9u1'                                      |
| libnghttp2-14    | Upgrade | '1.18.1-1+deb9u1' to '1.18.1-1+deb9u2'                            |
| libonig4         | Upgrade | '6.1.3-2' to '6.1.3-2+deb9u2'                                     |
| libp11-kit0      | Upgrade | '0.23.3-2' to '0.23.3-2+deb9u1'                                   |
| libpam-systemd   | Upgrade | '232-25+deb9u12' to '232-25+deb9u13'                              |
| libpcap0.8       | Upgrade | '1.8.1-3' to '1.8.1-3+deb9u1'                                     |
| libsnmp-base     | Upgrade | '5.7.3+dfsg-1.7+deb9u1' to '5.7.3+dfsg-1.7+deb9u3'                |
| libsnmp30        | Upgrade | '5.7.3+dfsg-1.7+deb9u1' to '5.7.3+dfsg-1.7+deb9u3'                |
| libsqlite3-0     | Upgrade | '3.16.2-5+deb9u1' to '3.16.2-5+deb9u3'                            |
| libss2           | Upgrade | '1.43.4-2+deb9u1' to '1.43.4-2+deb9u2'                            |
| libssh2-1        | Upgrade | '1.7.0-1+deb9u1' to '1.7.0-1+deb9u2'                              |
| libssl1.0.2      | Upgrade | '1.0.2u-1\~deb9u1' to '1.0.2u-1\~deb9u6'                          |
| libssl1.1        | Upgrade | '1.1.0l-1\~deb9u1' to '1.1.0l-1\~deb9u4'                          |
| libsystemd0      | Upgrade | '232-25+deb9u12' to '232-25+deb9u13'                              |
| libudev1         | Upgrade | '232-25+deb9u12' to '232-25+deb9u13'                              |
| libudisks2-0     | Upgrade | '2.1.8-1' to '2.1.8-1+deb9u1'                                     |
| libxml2          | Upgrade | '2.9.4+dfsg1-2.2+deb9u2' to '2.9.4+dfsg1-2.2+deb9u5'              |
| login            | Upgrade | '1:4.4-4.1' to '1:4.4-4.1+deb9u1'                                 |
| openssl          | Upgrade | '1.1.0l-1\~deb9u1' to '1.1.0l-1\~deb9u4'                          |
| passwd           | Upgrade | '1:4.4-4.1' to '1:4.4-4.1+deb9u1'                                 |
| rsyslog          | Upgrade | '8.24.0-1' to '8.24.0-1+deb9u1'                                   |
| snmp             | Upgrade | '5.7.3+dfsg-1.7+deb9u1' to '5.7.3+dfsg-1.7+deb9u3'                |
| snmpd            | Upgrade | '5.7.3+dfsg-1.7+deb9u1' to '5.7.3+dfsg-1.7+deb9u3'                |
| sudo             | Upgrade | '1.8.19p1-2.1+deb9u2' to '1.8.19p1-2.1+deb9u3'                    |
| systemd          | Upgrade | '232-25+deb9u12' to '232-25+deb9u13'                              |
| systemd-sysv     | Upgrade | '232-25+deb9u12' to '232-25+deb9u13'                              |
| tar              | Upgrade | '1.29b-1.1' to '1.29b-1.1+deb9u1'                                 |
| tzdata           | Upgrade | '2020a-0+deb9u1' to '2021a-0+deb9u2'                              |
| udev             | Upgrade | '232-25+deb9u12' to '232-25+deb9u13'                              |
| udhcpc           | Upgrade | '1:1.22.0-19+b3' to '1:1.22.0-19+deb9u2'                          |
| udisks2          | Upgrade | '2.1.8-1' to '2.1.8-1+deb9u1'                                     |
| wpasupplicant    | Upgrade | '2:2.4-1+deb9u6' to '2:2.4-1+deb9u9'                              |
| beep             | Remove  |                                                                   |
| bridge-utils     | Remove  |                                                                   |
| can-utils        | Remove  |                                                                   |
| curl             | Remove  |                                                                   |
| firmware-atheros | Remove  |                                                                   |
| ifenslave        | Remove  |                                                                   |
| ifenslave-2.6    | Remove  |                                                                   |
| iperf3           | Remove  |                                                                   |
| libiperf0        | Remove  |                                                                   |
| libjson-c-dev    | Remove  |                                                                   |
| libsapi-utils    | Remove  |                                                                   |
| libsapi0         | Remove  |                                                                   |
| tcpdump          | Remove  |                                                                   |

### Debian Security Patch

Refers to [UC-8410A MIL 1.1 Change Log](changelog/MIL110_UC-8410A_220302_045209.zip)


### Kernel Security Patch

Refer to below table for the list of kernel security patch

|                  |                |                |                |
| ---------------- | -------------- | -------------- | -------------- |
| CVE-2015-8553    | CVE-2019-18806 | CVE-2020-13974 | CVE-2021-26932 |
| CVE-2017-16644   | CVE-2019-19062 | CVE-2020-14305 | CVE-2021-27363 |
| CVE-2017-18509   | CVE-2019-19066 | CVE-2020-14314 | CVE-2021-27364 |
| CVE-2018-1000026 | CVE-2019-19068 | CVE-2020-14331 | CVE-2021-27365 |
| CVE-2018-10323   | CVE-2019-19318 | CVE-2020-14351 | CVE-2021-28038 |
| CVE-2018-13093   | CVE-2019-19319 | CVE-2020-14381 | CVE-2021-28964 |
| CVE-2018-19407   | CVE-2019-19332 | CVE-2020-14390 | CVE-2021-28972 |
| CVE-2018-20510   | CVE-2019-19447 | CVE-2020-14416 | CVE-2021-29650 |
| CVE-2018-20836   | CVE-2019-19448 | CVE-2020-15393 | CVE-2021-30002 |
| CVE-2018-20856   | CVE-2019-19523 | CVE-2020-15436 | CVE-2021-3178  |
| CVE-2019-0154    | CVE-2019-19524 | CVE-2020-15437 | CVE-2021-31916 |
| CVE-2019-10126   | CVE-2019-19525 | CVE-2020-16119 | CVE-2021-32399 |
| CVE-2019-10142   | CVE-2019-19527 | CVE-2020-16166 | CVE-2021-33034 |
| CVE-2019-10207   | CVE-2019-19528 | CVE-2020-1749  | CVE-2021-33909 |
| CVE-2019-10220   | CVE-2019-19530 | CVE-2020-24490 | CVE-2021-3428  |
| CVE-2019-10638   | CVE-2019-19531 | CVE-2020-24586 | CVE-2021-34693 |
| CVE-2019-10639   | CVE-2019-19532 | CVE-2020-25211 | CVE-2021-3564  |
| CVE-2019-11091   | CVE-2019-19533 | CVE-2020-25212 | CVE-2021-3573  |
| CVE-2019-11135   | CVE-2019-19534 | CVE-2020-25285 | CVE-2021-3587  |
| CVE-2019-11190   | CVE-2019-19535 | CVE-2020-25643 | CVE-2021-3609  |
| CVE-2019-1125    | CVE-2019-19536 | CVE-2020-25645 | CVE-2021-3612  |
| CVE-2019-11477   | CVE-2019-19537 | CVE-2020-25656 | CVE-2021-3653  |
| CVE-2019-11478   | CVE-2019-19767 | CVE-2020-25668 | CVE-2021-3655  |
| CVE-2019-11479   | CVE-2019-19768 | CVE-2020-25705 | CVE-2021-3659  |
| CVE-2019-11486   | CVE-2019-19813 | CVE-2020-26088 | CVE-2021-3679  |
| CVE-2019-11599   | CVE-2019-19816 | CVE-2020-26139 | CVE-2021-3715  |
| CVE-2019-11810   | CVE-2019-19965 | CVE-2020-26147 | CVE-2021-3732  |
| CVE-2019-11815   | CVE-2019-20054 | CVE-2020-26558 | CVE-2021-3753  |
| CVE-2019-11833   | CVE-2019-20096 | CVE-2020-27066 | CVE-2021-37576 |
| CVE-2019-11884   | CVE-2019-20636 | CVE-2020-27067 | CVE-2021-38160 |
| CVE-2019-12818   | CVE-2019-20810 | CVE-2020-2732  | CVE-2021-38204 |
| CVE-2019-12819   | CVE-2019-20812 | CVE-2020-27673 | CVE-2021-38205 |
| CVE-2019-13272   | CVE-2019-2101  | CVE-2020-27675 | CVE-2021-38208 |
| CVE-2019-14283   | CVE-2019-3459  | CVE-2020-27786 | CVE-2021-39634 |
| CVE-2019-14284   | CVE-2019-3460  | CVE-2020-27815 | CVE-2021-39657 |
| CVE-2019-14615   | CVE-2019-3846  | CVE-2020-27825 | CVE-2021-40490 |
| CVE-2019-14814   | CVE-2019-3882  | CVE-2020-28097 | CVE-2021-4157  |
| CVE-2019-14815   | CVE-2019-5108  | CVE-2020-28374 | CVE-2021-42008 |
| CVE-2019-14816   | CVE-2019-5489  | CVE-2020-28915 | CVE-2021-45485 |
| CVE-2019-14821   | CVE-2019-9213  | CVE-2020-28974 | CVE-2021-45486 |
| CVE-2019-14835   | CVE-2019-9445  | CVE-2020-29370 |                |
| CVE-2019-14895   | CVE-2019-9455  | CVE-2020-29371 |                |
| CVE-2019-14896   | CVE-2019-9503  | CVE-2020-29568 |                |
| CVE-2019-14897   | CVE-2019-9506  | CVE-2020-29660 |                |
| CVE-2019-14901   | CVE-2020-0009  | CVE-2020-29661 |                |
| CVE-2019-15212   | CVE-2020-0030  | CVE-2020-35508 |                |
| CVE-2019-15214   | CVE-2020-0255  | CVE-2020-35519 |                |
| CVE-2019-15216   | CVE-2020-0404  | CVE-2020-36312 |                |
| CVE-2019-15218   | CVE-2020-0427  | CVE-2020-36386 |                |
| CVE-2019-15219   | CVE-2020-0543  | CVE-2020-3702  |                |
| CVE-2019-15239   | CVE-2020-10135 | CVE-2020-8428  |                |
| CVE-2019-15292   | CVE-2020-10690 | CVE-2020-8647  |                |
| CVE-2019-15666   | CVE-2020-10711 | CVE-2020-8648  |                |
| CVE-2019-15807   | CVE-2020-10720 | CVE-2020-8649  |                |
| CVE-2019-15916   | CVE-2020-10732 | CVE-2020-8694  |                |
| CVE-2019-15917   | CVE-2020-10751 | CVE-2020-8992  |                |
| CVE-2019-15926   | CVE-2020-10766 | CVE-2020-9383  |                |
| CVE-2019-16413   | CVE-2020-10767 | CVE-2021-0129  |                |
| CVE-2019-16746   | CVE-2020-10768 | CVE-2021-0447  |                |
| CVE-2019-16995   | CVE-2020-10942 | CVE-2021-0448  |                |
| CVE-2019-17052   | CVE-2020-11565 | CVE-2021-0512  |                |
| CVE-2019-17053   | CVE-2020-12352 | CVE-2021-0920  |                |
| CVE-2019-17054   | CVE-2020-12464 | CVE-2021-0937  |                |
| CVE-2019-17055   | CVE-2020-12652 | CVE-2021-1048  |                |
| CVE-2019-17056   | CVE-2020-12656 | CVE-2021-20261 |                |
| CVE-2019-17133   | CVE-2020-12769 | CVE-2021-21781 |                |
| CVE-2019-17351   | CVE-2020-12770 | CVE-2021-22555 |                |
| CVE-2019-18282   | CVE-2020-12771 | CVE-2021-23133 |                |
| CVE-2019-18660   | CVE-2020-12826 | CVE-2021-23134 |                |
| CVE-2019-18805   | CVE-2020-13143 | CVE-2021-26931 |                |
